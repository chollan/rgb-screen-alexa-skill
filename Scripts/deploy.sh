#!/bin/bash

source ../.env
sam package --template-file template.yaml --output-template-file packaged.yaml --s3-bucket $S3_DELIVERY_NAME
aws cloudformation deploy --template-file packaged.yaml --stack-name $STACK_NAME --capabilities CAPABILITY_NAMED_IAM