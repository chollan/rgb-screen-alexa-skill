const alexa = require("alexa-app");
const app = new alexa.app("my screen");
const handlers = require('./handlers/general');

app.launch((request, response) => {
    response
        .say("Welcome to the RGB Matrix project.  You can use this skill to interact with the screen.  You can say things like 'tell my screen to Display the temperature'.  There are more things coming soon")
    ;
});

app.intent("DisplayCurrentTemperature", {
        "slots": {},
        "utterances": ["display the current temperature"]
    },
    handlers.handleDisplayCurrentTemperatureIntent
);

app.intent("DisplayANumber", {
        "slots": { "number": "AMAZON.NUMBER" },
        "utterances": ["display the number {-|number}"]
    },
    handlers.handleDisplayANumberIntent
);

app.intent('HelloPedro', handlers.handleHelloPedroIntent);

app.intent('SayHello', handlers.handleSayHelloIntent);

// connect the alexa-app to AWS Lambda
exports.handler = app.lambda();