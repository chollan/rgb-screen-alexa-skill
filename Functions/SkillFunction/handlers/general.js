const aws = require('aws-sdk');
const sqs = new aws.SQS();

module.exports = {

    handleHelloPedroIntent: async function(req, resp){
        await sqs.sendMessage({
            MessageBody: JSON.stringify({
                type: "message",
                fullText: "Hi Pedro!",
                seconds: 30
            }),
            QueueUrl: process.env.RPI_SQS_QUE_URL,
            MessageDeduplicationId: Math.random().toString(),
            MessageGroupId: "1"
        }).promise();

        return Promise.resolve(
            resp.say("Ok")
        );
    },


    handleSayHelloIntent: async function(req, resp){
        const name = req.slot('person');
        const nameCapitalized = name.charAt(0).toUpperCase() + name.slice(1);
        await sqs.sendMessage({
            MessageBody: JSON.stringify({
                type: "message",
                fullText: "Hi " + nameCapitalized + "!",
                seconds: 30
            }),
            QueueUrl: process.env.RPI_SQS_QUE_URL,
            MessageDeduplicationId: Math.random().toString(),
            MessageGroupId: "1"
        }).promise();

        return Promise.resolve(
            resp.say("Ok")
        );
    },


    handleDisplayCurrentTemperatureIntent: async function(req, resp){
        return Promise.resolve(
            resp.say("This is still being implemented.")
        );
    },


    handleDisplayANumberIntent: async function(req, resp){
        let number = req.slot("number");

        await sqs.sendMessage({
            MessageBody: JSON.stringify({
                type: "message",
                fullText: "Number: "+number,
                seconds: 10
            }),
            QueueUrl: process.env.RPI_SQS_QUE_URL,
            MessageDeduplicationId: Math.random().toString(),
            MessageGroupId: "1"
        }).promise();

        return Promise.resolve(
            resp.say("The number you said should be on the screen.")
        );
    }
};